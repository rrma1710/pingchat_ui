package com.cafe.pingchat;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class RecyclerViewAdapterChat extends RecyclerView.Adapter <RecyclerViewAdapterChat.MyViewHolder> {

    Context nContext;
    List<Chat> nData;


    public RecyclerViewAdapterChat(Context nContext, List<Chat> nData) {
        this.nContext = nContext;
        this.nData = nData;
    }

    @NonNull
    @Override
    public RecyclerViewAdapterChat.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(nContext).inflate(R.layout.item_chat, parent, false);
        RecyclerViewAdapterChat.MyViewHolder vHolder = new RecyclerViewAdapterChat.MyViewHolder(v);
        return vHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.tv_Name.setText(nData.get(position).getName());
        holder.tv_Data.setText(nData.get(position).getDeskipsi());
        holder.tv_Time.setText(nData.get(position).getWaktu());
        holder.tv_Pesan.setText(nData.get(position).getPesan());
        holder.iMg.setImageResource(nData.get(position).getPhoto());
    }

    @Override
    public int getItemCount() {
        return nData.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView tv_Name;
        private TextView tv_Data;
        private TextView tv_Time;
        private TextView tv_Pesan;
        private ImageView iMg;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            tv_Name = (TextView) itemView.findViewById(R.id.name_chat);
            tv_Data = (TextView) itemView.findViewById(R.id.data_chat);
            tv_Time = (TextView) itemView.findViewById(R.id.time_chat);
            tv_Pesan = (TextView)itemView.findViewById(R.id.pesan_chat);
            iMg = (ImageView) itemView.findViewById(R.id.img_chat);
        }
    }
}