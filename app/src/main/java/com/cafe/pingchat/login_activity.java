package com.cafe.pingchat;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class login_activity extends AppCompatActivity {
Button btn_;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_activity);

        btn_ = (Button)findViewById(R.id.btn_id);
        btn_.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               startActivity(new Intent(login_activity
               .this, MainActivity.class));
            }
        });

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Login");

    }

    public void onClick(View view) {
        startActivity(new Intent(login_activity.this, register_activity.class));
    }
}
