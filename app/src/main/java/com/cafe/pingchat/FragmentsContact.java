package com.cafe.pingchat;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class FragmentsContact extends Fragment {
    View view;
    private RecyclerView myrecyclerView;
    private List<Contact> lstContact;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.contact_fragment, container,false);
        myrecyclerView = (RecyclerView) view.findViewById(R.id.contact_recyclerView);
        RecyclerViewAdapterContact recyclerViewAdapter = new RecyclerViewAdapterContact(getContext(),lstContact);
        myrecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        myrecyclerView.setAdapter(recyclerViewAdapter);
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        lstContact = new ArrayList<>();
        lstContact.add(new Contact("Aliando","085330198921", R.drawable.aliando));
        lstContact.add(new Contact("Anang Hermansyah","085332762679", R.drawable.ananghermansyah));
        lstContact.add(new Contact("Andre","085789256354", R.drawable.andre));
        lstContact.add(new Contact("Anisa","08853514123", R.drawable.anisa));
        lstContact.add(new Contact("Ariel","085330680974", R.drawable.ariel));
        lstContact.add(new Contact("Ayu Ting Ting","085856725311", R.drawable.ayutingting));
        lstContact.add(new Contact("Aziz","085330112213", R.drawable.aziz));
        lstContact.add(new Contact("Bastian","08530078899", R.drawable.bastian));
        lstContact.add(new Contact("Ghozali","082335288172", R.drawable.ghozali));
        lstContact.add(new Contact("Raffi Ahmad","085330998382", R.drawable.raffiahmad));
        lstContact.add(new Contact("Ria Ricis","085333727616", R.drawable.riaricis));
        lstContact.add(new Contact("Sule","085456335278", R.drawable.sule));
        lstContact.add(new Contact("Syahrini","085330128394", R.drawable.syahrini));
        lstContact.add(new Contact("Vanessa","085330109347", R.drawable.vanessa));
    }
}
