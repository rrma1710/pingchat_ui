package com.cafe.pingchat;

public class Chat {
    private int Photo;
    private String Name;
    private String Deskipsi;
    private String waktu;
    private String pesan;

    public Chat(int photo, String name, String deskipsi, String waktu, String pesan) {
        Photo = photo;
        Name = name;
        Deskipsi = deskipsi;
        this.waktu = waktu;
        this.pesan = pesan;
    }

    public int getPhoto() {
        return Photo;
    }

    public void setPhoto(int photo) {
        Photo = photo;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getDeskipsi() {
        return Deskipsi;
    }

    public void setDeskipsi(String deskipsi) {
        Deskipsi = deskipsi;
    }

    public String getWaktu() {
        return waktu;
    }

    public void setWaktu(String waktu) {
        this.waktu = waktu;
    }

    public String getPesan() {
        return pesan;
    }

    public void setPesan(String pesan) {
        this.pesan = pesan;
    }

    public Chat() {
    }
}
