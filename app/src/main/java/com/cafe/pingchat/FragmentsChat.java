package com.cafe.pingchat;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class FragmentsChat extends Fragment {
    View view;
    private RecyclerView myrecyclerView;
    private List<Chat> lstChat;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.chat_fragment, container,false);
        myrecyclerView = (RecyclerView) view.findViewById(R.id.chat_recyclerView);
        RecyclerViewAdapterChat recyclerViewAdapterChat = new RecyclerViewAdapterChat(getContext(),lstChat);
        myrecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        myrecyclerView.setAdapter(recyclerViewAdapterChat);
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        lstChat = new ArrayList<>();
        lstChat.add(new Chat(R.drawable.ananghermansyah,"Anang Hermansyah","Hey Bro? Apa kabar?", "20:30","2"));
        lstChat.add(new Chat(R.drawable.bastian,"Bastian CJR","Hangoutan kawan? gua tunggu besok? OK???", "19:57","4"));
        lstChat.add(new Chat(R.drawable.ayutingting,"Ayu Ting Ting","Nanti jadwal Syuting kita jam berapa?", "20:10","3"));
        lstChat.add(new Chat(R.drawable.andre,"Andre Komedi","Lamborghninya gua Lepas aje lah Bro. Maunya Minta Berapa bro? 1M boleh langsung bungkus", "19:00","2"));
        lstChat.add(new Chat(R.drawable.sule,"Kang Sule","Bray, Ngopi nyang lampu merah kuy!!!", "18:10","3"));
        lstChat.add(new Chat(R.drawable.raffiahmad,"Raffi Ahmad","Besok gua Terbang bro ke amerika, lu ikutan ga?", "17:00","5"));
        lstChat.add(new Chat(R.drawable.ghozali,"ghozali","Lagu Seng digarap wes mari gan?", "16:55","3"));
        lstChat.add(new Chat(R.drawable.aziz,"Aziz Gagap","Gan, tar malem hangoutan sama kang sule bro? oyiii? ", "16:09","1"));
        lstChat.add(new Chat(R.drawable.riaricis,"Neng Ricis","Mas, Aku mau buat konten youtube besok. temenin ricis ya?", "15:50","8"));
        lstChat.add(new Chat(R.drawable.anisa,"Neng Anisa","A', Besok nisa mau beli ikan di pasar suruh mamah. ya? ikut..", "14.25","7"));
        lstChat.add(new Chat(R.drawable.ariel,"Ariel Noah","Woy Bang, Besok Perform di RT Sebelah, Lu ikutan ya?", "11.43","2"));
        lstChat.add(new Chat(R.drawable.vanessa,"Kak Vanessa","bang, nanti malam liat film dibioskop kuy!", "10.12","5"));
        lstChat.add(new Chat(R.drawable.syahrini,"Syahrini","Mas, Bulan Depan Neng ke Papua beli Sagu. ikut mas ya?", "09.00","3"));
        lstChat.add(new Chat(R.drawable.aliando,"Aliando","Bray, Ngopi nyang lampu merah kuy!!!", "00.01","4"));



    }
}
